

/* document.addEventListener("scroll", () => myFunction())
   let myFunction = function () {
   history.scrollRestoration = "manual"
}  */

//запрещает выделение мышкой и комбинации клавиш Ctrl + A и Ctrl + U и Ctrl + S
 function preventSelection(element) {
  var preventSelection = false;

  function addHandler(element, event, handler) {
    if (element.attachEvent) element.attachEvent("on" + event, handler);
    else if (element.addEventListener) element.addEventListener(event, handler, false);
  }

  function removeSelection() {
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
    } else if (document.selection && document.selection.clear)
      document.selection.clear();
  }

  //запрещаем выделять текст мышкой
  addHandler(element, "mousemove", function () {
    if (preventSelection) removeSelection();
  });
  addHandler(element, "mousedown", function (event) {
    var event = event || window.event;
    var sender = event.target || event.srcElement;
    preventSelection = !sender.tagName.match(/INPUT|TEXTAREA/i);
  });

  //запрещаем нажатие клавищ Ctrl + A и Ctrl + U и Ctrl + S
  function killCtrlA(event) {
    var event = event || window.event;
    var sender = event.target || event.srcElement;
    if (sender.tagName.match(/INPUT|TEXTAREA/i)) return;
    var key = event.keyCode || event.which;
    if ((event.ctrlKey && key == "U".charCodeAt(0)) || (event.ctrlKey && key == "A".charCodeAt(0)) || (event.ctrlKey && key == "S".charCodeAt(0))) // "A".charCodeAt(0) можно заменить на 65
    {
      removeSelection();
      if (event.preventDefault) event.preventDefault();
      else event.returnValue = false;
    }
  }
  addHandler(element, "keydown", killCtrlA);
  addHandler(element, "keyup", killCtrlA);
}
preventSelection(document);

//Запрещаем f12

/* document.oncontextmenu = function () {
  return false;
};
document.onkeydown = function (e) {
  if (e.keyCode == 123) {
    return false;
  }
  if (e.ctrlKey && e.shiftKey && e.keyCode == "I".charCodeAt(0)) {
    return false;
  }
  if (e.ctrlKey && e.shiftKey && e.keyCode == "J".charCodeAt(0)) {
    return false;
  }
  if (e.ctrlKey && e.keyCode == "U".charCodeAt(0)) {
    return false;
  }

  if (e.ctrlKey && e.shiftKey && e.keyCode == "C".charCodeAt(0)) {
    return false;
  }
} */






let lineX, lineY, divs, elem, arrayBlock = [];

function element() {
  lineY = document.documentElement.clientHeight / 100 * 15; // округлить до целого числа
  lineX = document.documentElement.clientWidth / 2;
  return elem = document.elementFromPoint(lineX, lineY);
}


let downHeader = document.getElementsByClassName("clon_header");
let block = document.getElementById("block");
window.onload = function getArrayPoint() {
  [].forEach.call(block.children, (elem) => {
    divs = elem.getBoundingClientRect()
    arrayBlock.push({
      name: elem,
      top: Math.round((divs.top) / 100) * 100
    });
  })
};

document.addEventListener("scroll", () => {
  element();
  let pointScroll = Math.round((pageYOffset + lineY) / 100) * 100;
  switch (pointScroll) {
    case 200:
      return downHeader[0].classList.add("header_active")

    case 100:
      return downHeader[0].classList.remove("header_active")

  }
})


  //***********************************************************************************************/
                              /// chenge a variables for clienWidth <= 1024 ///
                              
  if(document.body.clientWidth <= 1024){
    secondLine = 963; thirdLine = 1906;
   // document.getElementsByClassName("content_big anim-typewriter").innerHTML = "<h1>Мы предоставляем<br> маркетинговые и<br> IT-услуги в<br>Беларуси.</h1>"
    document.getElementsByClassName("famos_title_h4")[0].innerHTML = "<h4>Интернет-магазин/каталог товаров.</h4>"
    document.getElementsByClassName("famos_title_h6")[0].innerHTML = "<h6>Полноценный<br> интернет-магазин с корзиной</h6>"
    document.getElementsByClassName("famos_item_discription")[0].getElementsByTagName("li")[4].innerText = "Доработка и редизайн интернет-магазина"
    document.getElementsByClassName("famos_item_discription")[0].getElementsByTagName("li")[5].remove()
    document.getElementsByClassName("middle_title_h6")[0].innerHTML = "<h6>Landing page, сайт-визитка,<br> сайт-презентация, промо сайт</h6>"
    document.getElementsByClassName("famos_title_h4")[1].innerHTML = "<h4>Продвижение и реклама.</h4>"
    document.getElementsByClassName("famos_title_h6")[1].innerHTML = "<h6>Продвижение любых услуг с<br> помощью SEO, SMM и SMS</h6>"
    document.getElementsByClassName("famos_item_discription")[2].getElementsByTagName("li")[4].innerHTML = "Бесплатная консультация специалиста и<br> оценка проекта"
    document.getElementsByClassName("famos_item_discription")[2].getElementsByTagName("li")[5].remove()
    document.getElementsByClassName("process_discription")[0].innerHTML = "<h6>Чтобы приступить<br> к работе, нам нужно<br>"+
                                                                            "узнать как можно больше<br> о вашем бизнесе, после<br> подачи"+
                                                                            "заявки мы<br> отправляем вам анкету<br> с вопросами<br> и формируем задачу,<br>"+
                                                                            "стоимость и сроки<br> выполнения</h6>"
                                                                          
    document.getElementsByClassName("process_discription")[1].innerHTML = "<h6>Мы проводим<br> исследование вашей ЦА,<br> анализ кокурентов,<br>продумываем как<br> выделить ваш бизнес<br>на рынке услуг</h6>"
    document.getElementsByClassName("about_title_discont")[0].innerHTML = "<h3>Заполни форму<br>сейчас и получи<br>скидку на первый<br> заказ<span class = yellow> -10%!<span></h3>"                                                                          
  }                                                                           
  //***********************************************************************************************/
  
  if(document.body.clientWidth <= 768){
    secondLine = 963; thirdLine = 1906;
  document.getElementsByClassName("content_big")[0].innerHTML = "<h1>Мы предоставляем<br> маркетинговые<br>и IT-услуги<br> в Беларуси.</h1>"
  document.getElementsByClassName("About_title")[6].innerHTML = "<h4>Мы рады, когда наши клиенты оставляют отзывы о проделанной нами работе,<br> мы ценим каждого из вас. Спасибо!</h4>"
  document.getElementsByClassName("About_title")[7].innerHTML = "<h4>Заполните форму ниже и в течение 24 часов наш специалист свяжется с вами,<br> чтобы обсудить проект</h4>"
  document.getElementsByClassName("about_title_discont")[0].innerHTML = "<h3>Заполни форму сейчас и получи скидку<br> на первый заказ <span class = 'yellow'>-10%!<span><h3>"
  document.getElementsByClassName("famos_title_h6")[0].innerHTML = "<h6>Полноценный интернет-магазин с корзиной<h6>"
  document.getElementsByClassName("middle_title_h6")[0].innerHTML = "<h6>Landing page, сайт-визитка, сайт-презентация, промо сайт<h6>"
  document.getElementsByClassName("famos_title_h6")[1].innerHTML = "<h6>Продвижение любых услуг с помощью SEO, SMM и SMS<h6>"
  document.getElementsByClassName("middle_title_h4")[0].innerHTML = "<h4>Одностраничный сайт<h4>"
  document.getElementsByClassName("UNP")[0].innerText  = "УНП 491487135 ИП Мельников"
  }


  //***********************************************************************************************/
                              /// chenge a variables for clienWidth <= 375 ///
  if(document.body.clientWidth <= 375){
    secondLine = 963; thirdLine = 1906;                            
    document.getElementsByTagName("h2")[0].innerHTML = "<h2>Качественная разработка и доработка<br>сайтов, уникальный дизайн, настройка<br>рекламы и привлечение ЦА, приятные<br> бонусы и техподдержка 24/7.<h2>"
    document.getElementsByClassName("famos_title_h4")[0].innerHTML = "<h4>Интернет-магазин/<br>каталог товаров.</h4>";
    document.getElementsByClassName("famos_title_h6")[0].innerHTML = "<h6>Полноценный<br> интернет-магазин с корзиной<h6>";
    document.getElementsByClassName("middle_title_h4")[0].innerHTML = "<h4>Одностраничный<br>сайт.<h4>"
    document.getElementsByClassName("famos_item_discription")[0].getElementsByTagName("li")[4].innerHTML = "Доработка и редизайн<br>интернет-магазина"
    document.getElementsByClassName("middle_title_h6")[0].innerHTML = "<h6>Landing page, сайт-визитка,<br>сайт-презентация, промо сайт<h6>"
    document.getElementsByClassName("famos_item_discription")[1].getElementsByTagName("li")[6].innerHTML = "Не требуется дополнительной<br> поддержки"
    document.getElementsByClassName("famos_title_h4")[1].innerHTML = "<h4>Продвижение<br> и реклама.</h4>"
    document.getElementsByClassName("famos_title_h6")[1].innerHTML = "<h6>Продвижение любых услуг с<br> помощью SEO, SMM и SMS<h6>";
    document.getElementsByClassName("About_header")[4].getElementsByTagName("h2")[0].innerText = "Наши работы";
    document.getElementsByClassName("About_header")[6].getElementsByTagName("h2")[0].innerText = "Отзывы";
    document.getElementsByClassName("About_title")[7].innerHTML = "<h4>Заполните форму ниже и в течение 24 часов наш специалист свяжется с<br>вами, чтобы обсудить проект</h4>";
    let more_info = document.getElementsByClassName("more_info")[0];

    [].forEach.call(document.getElementsByClassName("develop"), (item,i) => {if(i >2 )item.classList.add("hidden")});

    more_info.addEventListener("click", function(){
       
      [].forEach.call(document.getElementsByClassName("develop"),(item,i) => { 
        item.classList.remove("hidden");
      });

      more_info.style = "display: none";

    }); 

  }
  //***********************************************************************************************/


    

                              // function for calculator
      let button = document.getElementsByClassName("button_counter")[0];
      let inputsChoice =  document.getElementsByClassName("choice");
      let inputsCalcut =  document.getElementsByClassName("calcut");
      let divCounter = document.getElementsByClassName("counter")[0];
      let show_result = document.getElementsByClassName("show_result")[0];
      let label = document.getElementsByClassName("secondLabel");
      let button_Back = document.getElementsByClassName("button_Back")[0];

      let dev = [{Name:"Верстка Landing Page",cost:"от 100 рублей"},{Name:"Адаптивная версткаx",cost:"от 110 рублей"},{Name:"Разработка интернет-магазинаx",cost:"от 120 рублей"},{Name:"Работа с базой данных",cost:"от 130 рублей"},{Name:"Доработка сайтовx",cost:"от 140 рублей"},{Name:"Комплексная разработка",cost:"от 100 рублей"}];
      let UX = [{Name:"Индивидуальный дизайн",cost:"от 100 рублей"},{Name:"Проектирование веб-сайтов",cost:"от 110 рублей"},{Name:"Дизайн Landing Page",cost:"от 120 рублей"},{Name:"UX/UI дизайн",cost:"от 130 рублей"},{Name:"Дизайн полиграфии",cost:"от 140 рублей"},{Name:"Брендинг и многое другое",cost:"от 100 рублей"}];
      let prom = [{Name:"SMM (Реклама в соцсетях)", cost:"от 100 рублей"},{Name:"SEO-продвижение",cost:"от 110 рублей"},{Name:"Viber и SMS-рассылка", cost:"от 120 рублей"},{Name:"Таргетинг", cost:"от 130 рублей"},{Name:"Контекстная реклама", cost:"от 140 рублей"},{Name:"Бесплатная консультация",cost:"от 100 рублей"}];
      let found = [{Name:"Анализ целевой аудитории",cost:"от 100 рублей"},{Name:"Привлечение новых клиентов",cost:"от 110 рублей"},{Name:"Оценка потребностей клиентов", cost:"от 120 рублей"},{Name:"Разработка под конкретную ЦА", cost:"от 130 рублей"},{Name:"Анализ конкурентов",cost:"от 140 рублей"},{Name:"Бесплатная консультация",cost:"от 100 рублей"}];
      let video = [{Name:"Съемка видеороликов",cost:"от 100 рублей"},{Name:"Видео-дизайн",cost:"от 110 рублей"},{Name:"Креативная реклама", cost:"от 120 рублей"},{Name:"Раскрутка в YouTube", cost:"от 130 рублей"},{Name:"Генерация креативных идей для", cost:"от 140 рублей"},{Name:"Видео визитка",cost:"от 100 рублей"}];
      let support = [{Name:"Помощь выбора домена",cost:"от 100 рублей"},{Name:"Круглосуточная техподдержка",cost:"от 110 рублей"},{Name:"Исправление ошибок на сайте", cost:"от 120 рублей"},{Name:"Помощь с формами", cost:"от 130 рублей"},{Name:"Помощь в размещении контента",cost:"от 140 рублей"},{Name:"Бесплатная консультация",cost:"от 100 рублей"}];

      let showChoice = function(){ 
            [].forEach.call(inputsChoice, (i) => {if(i.checked){setTwo(i.id)} })                      
      }
      let showCalcut = function(){ 
            [].forEach.call(inputsCalcut, (i) => {if(i.checked){
                   show_result.getElementsByTagName("span")[0].innerText = i.cost;
                   show_result.getElementsByTagName("span")[0].style = "color: #0023FF";
                 }
               })                      
      }

      button.addEventListener("click", showChoice);
      [].forEach.call(inputsCalcut, (i) => i.addEventListener("click", showCalcut));
       
      let creatForm = function(id,div){
        [].forEach.call(inputsCalcut, (elem,index ) =>{elem.name = id, elem.value = div[index]["Name"], elem.cost = div[index]["cost"]});
        [].forEach.call(label, (elem, index)=>{elem.innerHTML = "<h6>"+div[index]["Name"]+"<h6>"})
      }


      let setTwo = function(id){
                                    /// скрываем первую форму и кнопку вперед
          document.getElementsByClassName("first_form")[0].classList.add("hidden");
          document.getElementsByClassName("button_counter")[0].classList.add("hidden");

                                    /// показываем вторую форму 
          document.getElementsByClassName("second_form")[0].classList.remove("hidden");
          
                                    /// показываем кнопки назад и заказ
          document.getElementsByClassName("button_row")[0].classList.remove("hidden");
          document.getElementsByClassName("button_Back")[0].classList.remove("hidden");
          document.getElementsByClassName("button_result")[0].classList.remove("hidden");

                                     /// показываем результат
          show_result.classList.remove("hidden");

                                     /// показываем линию прогресса и background 2
          document.getElementsByClassName("disprogress")[0].classList.remove("disprogress")
          divCounter.style = 'background-image: url("pictures/test_background2.png")';  


          switch(id){
               case "dev":
                     
                  return  creatForm(id,dev);

               case "UX": 

                 return  creatForm(id,UX);

               case "prom":
                     
                 return  creatForm(id,prom)

               case "found":

                 return  creatForm(id,found)


               case "video":

                 return  creatForm(id,video)

               case "support": 

                 return  creatForm(id,support)   

               default: console.log("nathing")  

          }
      };
      
                                //// возврат к первой форме
      let BackFirstForm = function(){
                                    /// скрываем первую форму и кнопку вперед
                                    document.getElementsByClassName("first_form")[0].classList.remove("hidden");
                                    document.getElementsByClassName("button_counter")[0].classList.remove("hidden");
                          
                                                              /// показываем вторую форму 
                                    document.getElementsByClassName("second_form")[0].classList.add("hidden");
                                    
                                                              /// показываем кнопки назад и заказ
                                    document.getElementsByClassName("button_row")[0].classList.add("hidden");
                                    document.getElementsByClassName("button_Back")[0].classList.add("hidden");
                                    document.getElementsByClassName("button_result")[0].classList.add("hidden");
                          
                                                               /// показываем результат
                                    show_result.classList.add("hidden");
                          
                                                               /// показываем линию прогресса и background 2
                                    document.getElementsByClassName("line_progress")[1].classList.add("disprogress")
                                    divCounter.style = 'background-image: url("pictures/test_background1.png")';  
      }
                      /// кнопка назад клик
      button_Back.addEventListener("click", BackFirstForm)

 /*****************************************************************************************************************/ 
                                              /* form colback */
        let customerName = document.getElementsByClassName("name")[0].value
        let customerNumber = document.getElementsByClassName("number")[0].value
        document.getElementsByClassName("two")[1].addEventListener("click",() =>{
          console.log(document.getElementsByClassName("name")[0].value,document.getElementsByClassName("number")[0].value); 
          fetch("http://localhost:3000/",{
                                                "headers": {
                                                  "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                                  "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
                                                  "cache-control": "no-cache",
                                                  "content-type": "application/x-www-form-urlencoded",
                                                  "pragma": "no-cache",
                                                  "sec-fetch-dest": "document",
                                                  "sec-fetch-mode": "navigate",
                                                  "sec-fetch-site": "same-origin",
                                                  "sec-fetch-user": "?1",
                                                  "upgrade-insecure-requests": "1"
                                                },
                                               
                                                "referrerPolicy": "no-referrer-when-downgrade",
                                                "body": "userName= "+document.getElementsByClassName("name")[0].value+"&userPhone="+document.getElementsByClassName("number")[0].value+"&userComment="+document.getElementsByClassName("comment")[0].value,
                                                "method": "POST",
                                                "mode": "no-cors"
                                              });
                            },
          )  
        

          // ****************************************mobil menu animation***************************************** //

          // show menu 357                      
          document.getElementsByClassName("humburger_menu")[0].addEventListener("click",() => {
                   document.body.classList.add("overflow_hidden");
                   document.getElementsByClassName("sail_menu")[0].classList.add("sail_menu_active")
                   
          })
 
          // closed menu 357
          document.getElementsByClassName("sail_menu_closed")[0].addEventListener("click",() => {
                   document.body.classList.remove("overflow_hidden");
                   document.getElementsByClassName("sail_menu_active")[0].classList.remove("sail_menu_active");
                   
          })

          // **********************************************move to links for sites*************************************************
          var linkNav = document.querySelectorAll('[href^="#"]');
            V = 0.5;  // скорость
            for (var i = 0; i < linkNav.length; i++) {
              if(i <= 4){linkNav[i].addEventListener("click",() => document.getElementsByClassName("sail_menu_active")[0].classList.remove("sail_menu_active") )}
              linkNav[i].onclick = function(){
                document.body.classList.remove("overflow_hidden");
                var w = window.pageYOffset,
                    hash = this.href.replace(/[^#]*(.*)/, '$1');
                    t = document.querySelector(hash).getBoundingClientRect().top,
                    start = null;
                requestAnimationFrame(step);
                function step(time) {
                  if (start === null) start = time;
                  var progress = time - start,
                      r = (t < 0 ? Math.max(w - progress/V, w + t) : Math.min(w + progress/V, w + t));
                  window.scrollTo(0,r);
                  if (r != w + t) {requestAnimationFrame(step)} else {location.hash = hash}
                }
                return false;
              }
            }